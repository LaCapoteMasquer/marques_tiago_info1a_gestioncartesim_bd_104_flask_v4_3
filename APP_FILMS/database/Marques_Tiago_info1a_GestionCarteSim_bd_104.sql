-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 26 mai 2021 à 18:18
-- Version du serveur :  10.4.18-MariaDB
-- Version de PHP : 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `marques_tiago_info1a_gestioncartesim_bd_104`
--
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS Marques_Tiago_INFO1A_GestionCarteSim_BD_104;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS Marques_Tiago_INFO1A_GestionCarteSim_BD_104;

-- Utilisation de cette base de donnée

USE Marques_Tiago_INFO1A_GestionCarteSim_BD_104;
-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_sim_pc`
--

CREATE TABLE `t_avoir_sim_pc` (
  `ID_Avoir_Sim_PC` int(11) NOT NULL,
  `FK_Sim` int(11) NOT NULL,
  `FK_PC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_avoir_sim_pc`
--

INSERT INTO `t_avoir_sim_pc` (`ID_Avoir_Sim_PC`, `FK_Sim`, `FK_PC`) VALUES
(1, 2, 1),
(2, 3, 2),
(3, 6, 7),
(4, 6, 9),
(6, 5, 2),
(7, 7, 6);

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_sim_telephone`
--

CREATE TABLE `t_avoir_sim_telephone` (
  `ID_Avoir_Sim_Telephone` int(11) NOT NULL,
  `FK_Sim` int(11) NOT NULL,
  `FK_Telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_avoir_sim_telephone`
--

INSERT INTO `t_avoir_sim_telephone` (`ID_Avoir_Sim_Telephone`, `FK_Sim`, `FK_Telephone`) VALUES
(1, 5, 2),
(2, 5, 8),
(3, 7, 9),
(4, 7, 10),
(5, 4, 9),
(6, 8, 1),
(7, 10, 7),
(8, 11, 8);

-- --------------------------------------------------------

--
-- Structure de la table `t_employe`
--

CREATE TABLE `t_employe` (
  `ID_Employe` int(11) NOT NULL,
  `Nom` varchar(30) NOT NULL,
  `Prenom` varchar(30) NOT NULL,
  `Date_de_naissance` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_employe`
--

INSERT INTO `t_employe` (`ID_Employe`, `Nom`, `Prenom`, `Date_de_naissance`) VALUES
(1, 'Marques', 'Tiago', '2003-02-25'),
(2, 'Lothbrok', 'Björn', '2002-07-21'),
(3, 'Lothbrok', 'Ragnar', '1960-02-20'),
(4, 'Côte-de-fer', 'Sigûr', '2000-04-10'),
(5, 'Le Désossé ', 'Ivar', '2006-03-30'),
(6, 'Lothbrok', 'Rolo', '1955-08-15'),
(7, 'Roger', 'Steve', '1940-08-10'),
(8, 'Stark', 'Tony', '1980-10-02'),
(9, 'Fils d\'odin', 'Thor', '1996-12-19'),
(10, 'Fils d\'odin', 'Loki', '1987-11-17');

-- --------------------------------------------------------

--
-- Structure de la table `t_pc`
--

CREATE TABLE `t_pc` (
  `ID_PC` int(11) NOT NULL,
  `Modele_PC` varchar(30) NOT NULL,
  `Numero_de_serie_PC` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_pc`
--

INSERT INTO `t_pc` (`ID_PC`, `Modele_PC`, `Numero_de_serie_PC`) VALUES
(1, 'dell', 192849428392),
(2, 'Dell 7490', 20958138421),
(3, 'Asus tuf ', 98746382145),
(4, 'Acer Predator orion 9000', 97654812458),
(5, 'Hp elitebook', 6857749581),
(6, 'Hp probook', 4576921583),
(7, 'Asus rog Zephyrus', 13548576492),
(8, 'Acer Predator orion 5000', 45796821365),
(9, 'Asus tuf ', 7485961235),
(10, 'Acer nitro 5', 123654748596);

-- --------------------------------------------------------

--
-- Structure de la table `t_preter_pc`
--

CREATE TABLE `t_preter_pc` (
  `ID_Preter_Materiel` int(11) NOT NULL,
  `FK_PC` int(11) NOT NULL,
  `FK_Employe` int(11) NOT NULL,
  `Date_de_pret` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_preter_telephone`
--

CREATE TABLE `t_preter_telephone` (
  `ID_Preter_Telephone` int(11) NOT NULL,
  `FK_Telephone` int(11) NOT NULL,
  `FK_Employe` int(11) NOT NULL,
  `Date_de_pret` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_preter_telephone`
--

INSERT INTO `t_preter_telephone` (`ID_Preter_Telephone`, `FK_Telephone`, `FK_Employe`, `Date_de_pret`) VALUES
(1, 4, 4, '2021-02-22 08:57:04'),
(2, 1, 2, '2021-02-22 08:57:04'),
(3, 6, 10, '2021-02-22 08:57:50'),
(4, 10, 10, '2021-02-22 08:57:50');

-- --------------------------------------------------------

--
-- Structure de la table `t_rendre_pc`
--

CREATE TABLE `t_rendre_pc` (
  `ID_rendre_materiel` int(11) NOT NULL,
  `FK_PC` int(11) NOT NULL,
  `FK_Employe` int(11) NOT NULL,
  `Date_de_retour` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_rendre_telephone`
--

CREATE TABLE `t_rendre_telephone` (
  `ID_Rendre_Telephone` int(11) NOT NULL,
  `FK_Telephone` int(11) NOT NULL,
  `FK_Employe` int(11) NOT NULL,
  `Date_de_retour` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_rendre_telephone`
--

INSERT INTO `t_rendre_telephone` (`ID_Rendre_Telephone`, `FK_Telephone`, `FK_Employe`, `Date_de_retour`) VALUES
(1, 4, 4, '2021-02-22 08:56:41'),
(2, 1, 2, '2021-02-22 08:56:41'),
(3, 6, 10, '2021-02-22 08:58:02'),
(4, 10, 10, '2021-02-22 08:58:02');

-- --------------------------------------------------------

--
-- Structure de la table `t_sim`
--

CREATE TABLE `t_sim` (
  `ID_Sim` int(11) NOT NULL,
  `Modele` varchar(200) DEFAULT NULL,
  `numero_de_serie` varchar(200) DEFAULT NULL,
  `Date_achat` date DEFAULT NULL,
  `Pin1` int(10) DEFAULT NULL,
  `Pin2` int(10) DEFAULT NULL,
  `numero_de_telephone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_sim`
--

INSERT INTO `t_sim` (`ID_Sim`, `Modele`, `numero_de_serie`, `Date_achat`, `Pin1`, `Pin2`, `numero_de_telephone`) VALUES
(2, 'multi-device', '9685485612563', '2020-10-21', 6548, 2525252, '786584557'),
(3, 'Data', '78965854741', '2020-11-11', 0, 1236987, '784659825'),
(4, 'Telephone', '5468579584682', '2021-02-14', 5555, 6548475, '782130524'),
(5, 'Multi-Device', '125467586954', '2020-05-14', 6666, 4785951, '780025466'),
(6, 'Data', '4548569964', '2020-04-14', 4569, 2525252, '782364985'),
(7, 'Multi-Device', '5487696585', '2019-07-10', 3210, 1236548, '781458747'),
(8, 'Telephone', '7485964758', '2021-01-13', 3574, 1597534, '789143718'),
(10, 'Multi-Device', '7648590125', '2021-02-14', 379, 9405348, '789696458'),
(11, 'Telephone', '578941350', '2021-02-04', 4358, 7548210, '780245718'),
(19, 'mako', '666', '2021-05-05', 456, 45646, '4456');


-- --------------------------------------------------------

--
-- Structure de la table `t_telephone`
--

CREATE TABLE `t_telephone` (
  `ID_telephone` int(11) NOT NULL,
  `Modele_telephone` varchar(30) NOT NULL,
  `numero_de_serie_telephone` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_telephone`
--

INSERT INTO `t_telephone` (`ID_telephone`, `Modele_telephone`, `numero_de_serie_telephone`) VALUES
(1, 'Iphone 6', 24576617652452),
(2, 'Iphone 6 plus', 65447958249215),
(3, 'Iphone 7', 346957485769),
(4, 'Iphone XS', 2135487928475),
(5, 'Iphone 11', 546985858585),
(6, 'Iphone 8', 15951753357),
(7, 'Iphone 7', 357485859696),
(8, 'Iphone XS', 35741478520),
(9, 'Iphone 11', 4563217890),
(10, 'Iphone 8', 9685741425360);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `t_avoir_sim_pc`
--
ALTER TABLE `t_avoir_sim_pc`
  ADD PRIMARY KEY (`ID_Avoir_Sim_PC`),
  ADD KEY `FK_Sim` (`FK_Sim`),
  ADD KEY `FK_PC` (`FK_PC`);

--
-- Index pour la table `t_avoir_sim_telephone`
--
ALTER TABLE `t_avoir_sim_telephone`
  ADD PRIMARY KEY (`ID_Avoir_Sim_Telephone`),
  ADD KEY `FK_Sim` (`FK_Sim`),
  ADD KEY `FK_Telephone` (`FK_Telephone`);

--
-- Index pour la table `t_employe`
--
ALTER TABLE `t_employe`
  ADD PRIMARY KEY (`ID_Employe`);

--
-- Index pour la table `t_pc`
--
ALTER TABLE `t_pc`
  ADD PRIMARY KEY (`ID_PC`);

--
-- Index pour la table `t_preter_pc`
--
ALTER TABLE `t_preter_pc`
  ADD PRIMARY KEY (`ID_Preter_Materiel`),
  ADD KEY `FK_Employe` (`FK_Employe`),
  ADD KEY `FK_PC` (`FK_PC`);

--
-- Index pour la table `t_preter_telephone`
--
ALTER TABLE `t_preter_telephone`
  ADD PRIMARY KEY (`ID_Preter_Telephone`),
  ADD KEY `FK_Telephone` (`FK_Telephone`),
  ADD KEY `FK_Employe` (`FK_Employe`);

--
-- Index pour la table `t_rendre_pc`
--
ALTER TABLE `t_rendre_pc`
  ADD PRIMARY KEY (`ID_rendre_materiel`),
  ADD KEY `FK_Employe` (`FK_Employe`),
  ADD KEY `FK_PC` (`FK_PC`);

--
-- Index pour la table `t_rendre_telephone`
--
ALTER TABLE `t_rendre_telephone`
  ADD PRIMARY KEY (`ID_Rendre_Telephone`),
  ADD KEY `FK_Telephone` (`FK_Telephone`),
  ADD KEY `FK_Employe` (`FK_Employe`);

--
-- Index pour la table `t_sim`
--
ALTER TABLE `t_sim`
  ADD PRIMARY KEY (`ID_Sim`);

--
-- Index pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
  ADD PRIMARY KEY (`ID_telephone`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `t_avoir_sim_pc`
--
ALTER TABLE `t_avoir_sim_pc`
  MODIFY `ID_Avoir_Sim_PC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `t_avoir_sim_telephone`
--
ALTER TABLE `t_avoir_sim_telephone`
  MODIFY `ID_Avoir_Sim_Telephone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `t_employe`
--
ALTER TABLE `t_employe`
  MODIFY `ID_Employe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `t_pc`
--
ALTER TABLE `t_pc`
  MODIFY `ID_PC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `t_preter_pc`
--
ALTER TABLE `t_preter_pc`
  MODIFY `ID_Preter_Materiel` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_preter_telephone`
--
ALTER TABLE `t_preter_telephone`
  MODIFY `ID_Preter_Telephone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_rendre_pc`
--
ALTER TABLE `t_rendre_pc`
  MODIFY `ID_rendre_materiel` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_rendre_telephone`
--
ALTER TABLE `t_rendre_telephone`
  MODIFY `ID_Rendre_Telephone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_sim`
--
ALTER TABLE `t_sim`
  MODIFY `ID_Sim` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
  MODIFY `ID_telephone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_avoir_sim_pc`
--
ALTER TABLE `t_avoir_sim_pc`
  ADD CONSTRAINT `t_avoir_sim_pc_ibfk_1` FOREIGN KEY (`FK_Sim`) REFERENCES `t_sim` (`ID_Sim`),
  ADD CONSTRAINT `t_avoir_sim_pc_ibfk_2` FOREIGN KEY (`FK_PC`) REFERENCES `t_pc` (`ID_PC`);

--
-- Contraintes pour la table `t_avoir_sim_telephone`
--
ALTER TABLE `t_avoir_sim_telephone`
  ADD CONSTRAINT `t_avoir_sim_telephone_ibfk_1` FOREIGN KEY (`FK_Sim`) REFERENCES `t_sim` (`ID_Sim`),
  ADD CONSTRAINT `t_avoir_sim_telephone_ibfk_2` FOREIGN KEY (`FK_Telephone`) REFERENCES `t_telephone` (`ID_telephone`);

--
-- Contraintes pour la table `t_preter_pc`
--
ALTER TABLE `t_preter_pc`
  ADD CONSTRAINT `t_preter_pc_ibfk_1` FOREIGN KEY (`FK_PC`) REFERENCES `t_pc` (`ID_PC`),
  ADD CONSTRAINT `t_preter_pc_ibfk_2` FOREIGN KEY (`FK_Employe`) REFERENCES `t_employe` (`ID_Employe`);

--
-- Contraintes pour la table `t_preter_telephone`
--
ALTER TABLE `t_preter_telephone`
  ADD CONSTRAINT `t_preter_telephone_ibfk_1` FOREIGN KEY (`FK_Telephone`) REFERENCES `t_telephone` (`ID_telephone`),
  ADD CONSTRAINT `t_preter_telephone_ibfk_2` FOREIGN KEY (`FK_Employe`) REFERENCES `t_employe` (`ID_Employe`);

--
-- Contraintes pour la table `t_rendre_pc`
--
ALTER TABLE `t_rendre_pc`
  ADD CONSTRAINT `t_rendre_pc_ibfk_1` FOREIGN KEY (`FK_PC`) REFERENCES `t_pc` (`ID_PC`),
  ADD CONSTRAINT `t_rendre_pc_ibfk_2` FOREIGN KEY (`FK_Employe`) REFERENCES `t_employe` (`ID_Employe`);

--
-- Contraintes pour la table `t_rendre_telephone`
--
ALTER TABLE `t_rendre_telephone`
  ADD CONSTRAINT `t_rendre_telephone_ibfk_1` FOREIGN KEY (`FK_Telephone`) REFERENCES `t_telephone` (`ID_telephone`),
  ADD CONSTRAINT `t_rendre_telephone_ibfk_2` FOREIGN KEY (`FK_Employe`) REFERENCES `t_employe` (`ID_Employe`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
